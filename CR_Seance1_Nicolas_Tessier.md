# Compte rendu Séance du 26/10

## Ce que j'ai appris :
• L'existence du ventirad, je pensais qu'il y avait que les ventilateurs normaux
• Quoi démonter/monter en premier sur un PC fixe
• Un peu plus de détails sur l'utilité de certains composants


## Ce que j'ai aimé : 
• Le cours théorique (simple et basique pour un premier cours, c'est top) 
• Démonter le PC (c'est sympa à faire) 
• La satisfaction quand le PC s'allume une fois remonté


## Ce que j'ai moins aimé : 
• Remonter le PC mais c'est temporaire, ce n'est pas évident la 1ere fois dû au nombre de câbles et de branchements différents  


## Attentes pour la suite : 
• Un cours pour mieux appréhender les branchements
• Apprendre à identifier un composant qui ne fonctionne pas et qui par exemple bloque l'allumage du PC

