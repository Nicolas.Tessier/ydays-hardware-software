# Compte rendu Séance du 16/11

## Ce que j'ai appris :
• Les connectiques de la carte mère


## Ce que j'ai aimé : 
• Manipuler la carte mère


## Ce que j'ai moins aimé : 
• La durée du tp avec la carte mère : trop simple et trop court 
• Le tp de config avec les filières ynov : avec un budget élevé et des besoins qui se ressemblent, on finit avec des builds qui se ressemblent aussi, ça aurait été plus intéressant d’avoir des budgets différents et des besoins plus différents/spécifiques 


## Attentes pour la suite : 
• Apprendre à faire des choix et mettre des priorités sur des composants plutôt que d’autres par rapport à un besoin et un budget

Lien du tp : https://hackmd.io/K0htfmk6QwuyfM52MFsCJA

